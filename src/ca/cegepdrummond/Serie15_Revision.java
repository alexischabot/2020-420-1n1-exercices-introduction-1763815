package ca.cegepdrummond;

import java.util.Scanner;

public class Serie15_Revision {
    /*
     *  Modifiez le code pour répondre à l'algorithme suivant qui détermine si 2 mots sont des anagrammes
     *     (2 mots composés des mêmes lettres dans un ordre différent)
     *
     * Note: pour simplifier le problème, nous ne vérifierons pas pour les accents.
     *       Ex: argent et gérant ne seront pas des anagramme, malgré le fait que seul l'accent est différent
     *
     *     Lire 2 chaines de caractères
     *     Si leurs longueurs sont différentes, ce ne sont pas des anagrammes: terminer le programme
     *          et afficher "pas un anagramme".
     *     Mettre les deux chaines en minuscules
     *     Transférer la première dans un tableau de caractères (char[ ])
     *     Pour chacun des caractères de la deuxième chaine
     *        vérifier si ce caractère est dans le tableau
     *        si oui, remplacer ce caractère dans le tableau par un espace (ou tout autre caractère non-valide)
     *            pourquoi faire ça? ... de cette façon on s'assure qu'on n'utilise pas deux fois
     *                la même lettre dans la tableau.
     *        si non, ce n'est pas un anagramme: terminer le programme et afficher "pas un anagramme"
     *
     *      si on a vérifié toutes les lettres de la deuxième chaine et qu'on les a toutes trouvées dans le tableau,
     *           alors on a un anagramme: afficher "anagramme"
     *
     *
     * exemple 1
     * aube
     * beau
     * affichera:
     * anagramme
     *
     * Exemple 2
     * chien
     * niche
     * affichera:
     * anagramme
     *
     * exemple 3
     * toto
     * titi
     * affichera
     * pas un anagramme
     *
     * exemple 4
     * aaaa
     * aaaa
     * affichera
     * anagramme
     */
    public void revision1() {
        Scanner s = new Scanner(System.in);

        String a = s.next();
        String b = s.next();
        boolean anagramme = true;
        char[] t = new char[a.length()];
        int i = 0;

        a = a.toLowerCase();
        b = b.toLowerCase();

        if (a.length() != b.length()) {
            anagramme = false;
        } else {
            for (int x = 0; x < t.length; x++) {
                t[x] = a.charAt(x);
            }

            while (i < b.length()) {
                int j = 0;
                while (j < t.length) {
                    if (b.charAt(i) == t[j]) {
                        t[j] = ' ';
                        j = b.length();
                    } else if (j == b.length() - 1 && t[j] != b.charAt(i)) {
                        anagramme = false;
                    }
                    j++;
                }
                i++;
            }
        }

        if (anagramme) {
            System.out.println("anagramme");
        } else if (!anagramme) {
            System.out.println("pas un anagramme");
        }

    }


    /*
     * Le pseudocode suivant permet de trier (de façon très peu efficace) un tableau d'entiers positifs.
     * Vous devez coder une fonction suivant cet algorithme.

     * Lire le tableau d'entiers (ce code est fourni)
     *     Le premier entier est le nombre d'éléments à entrer.
     *
     * Créer un tableau de résultat ayant la même taille que le tableau en entrée.
     * Initialiser le tableau de résultat avec des 0.
     * Créer la variable entière valeurMin
     * Créer la variable entière indiceAconserver
     * Créer la variable entière prochainePosition  = 0
     * Boucler de 0 à la longueur du tableau d'entrée
     *     valeurMin = Integer.MAX_VALUE
     *     Boucler (boucle dans une boucle) de 0 à la longueur du tableau d'entrée
     *        (appelez la variable de cette boucle "positionVerifiee")
     *        Si la valeur dans le tableau d'entrée à l'indice positionVerifiee  est plus petit ou égale à valeurMin
     *            mettre cette valeur dans valeurMin
     *            mettre indiceAconserver égale à l'indice de la boucle (c.à.d. positionVerifiee)
     *    Mettre valeurMin dans résultat à la position prochainePosition
     *    Dans le tableau d'entrée, mettre la valeur Integer.MAX_VALUE dans l'élément à la position indiceAconserver
     *        (Cela permet de ne pas réutiliser cette valeur lors de la prochaine itération)
     *    Augmenter prochainePosition de 1
     *
     * Afficher le tableau de résultat (ce code est fourni)
     */
    public void revision2() {
        Scanner s = new Scanner(System.in);
        int nombreElement = s.nextInt();
        int[] tableau = new int[nombreElement];
        int[] resultat = new int[nombreElement];
        for (int i = 0; i < nombreElement; i++) {
            tableau[i] = s.nextInt();
            resultat[i] = 0;
        }
        int valeurMin;
        int indiceAconserver = 0;

        for (int prochainePos = 0; prochainePos < nombreElement; prochainePos++) {
            valeurMin = Integer.MAX_VALUE;
            for (int posVerifiee = 0; posVerifiee < nombreElement; posVerifiee++) {
                if (tableau[posVerifiee] <= valeurMin) {
                    valeurMin = tableau[posVerifiee];
                    indiceAconserver = posVerifiee;
                }
            }
            resultat[prochainePos] = valeurMin;
            tableau[indiceAconserver] = Integer.MAX_VALUE;
        }
        for(int i = 0; i < nombreElement; i++) {
            System.out.print(resultat[i] + " ");
        }
        System.out.println("");
    }

}
